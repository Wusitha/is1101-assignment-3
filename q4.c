#include<stdio.h>
int main()
{
	int num;
	//get input
	printf("Enter number: ");
	scanf("%d", & num);

	if(num > 0)
	{
		printf("Factors of %d. \n", num);

		for(int i = 1; i <= num; i++)
		{
			if(num % i == 0)
			{
				printf("%d ,", i);
			}
		}

		printf("\n");
	}
	else
	{
		printf("Factors not found.");
	}

	return 0;
}
